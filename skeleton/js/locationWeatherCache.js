// Code for LocationWeatherCache class and other shared code.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "weatherApp";


function LocationWeatherCache()
{
    // Private attributes:

    var locations = [];
    var callbacks = {};

    // Public methods:

    // Returns the number of locations stored in the cache.
    this.length = function() {
        return locations.length;
    };

    // Returns the location object for a given index.
    // Indexes begin at zero.
    this.locationAtIndex = function(index) {
        return locations[index];
    };

    // Given a latitude, longitude and nickname, this method saves a
    // new location into the cache.  It will have an empty 'forecasts'
    // property.  Returns the index of the added location.
    //
    this.addLocation = function(latitude, longitude, nickname)
    {
        var newLocation = {
            lat : latitude,
            lng : longitude,
            nick : nickname,
            forecasts : {},
        };
        
        locations.push(newLocation) //push the new location to the locations array
        
        index = locations.length - 1 //the index number will decrease per one everytime as it goes
        
        saveLocations() //call the save location function to store it
        
        return index //get the index number
    }

    // Removes the saved location at the given index.
    //
    this.removeLocationAtIndex = function(index)
    {        
        cutLocation = locations.splice(index,1); //pick the value from the array depending the index number
        
        var resaveLocation = JSON.stringify(this); //resave it again by stringifying it
        
        //make sure the local storage is available, else display alert
        if (localStorage===null){
        alert("local storage is currently not available");
    }
    else {
     localStorage.setItem(APP_PREFIX,resaveLocation); 
    }
    }

    // This method is used by JSON.stringify() to serialise this class.
    // Note that the callbacks attribute is only meaningful while there
    // are active web service requests and so doesn't need to be saved.
    //
    this.toJSON = function() {
        
        var locationToJSON = locations; 
        
        return locationToJSON
    };

    // Given a public-data-only version of the class (such as from
    // local storage), this method will initialise the current
    // instance to match that version.
    //
    this.initialiseFromPDO = function(locationWeatherCachePDO) {
        locations = locationWeatherCachePDO;
    };

    // Request weather for the location at the given index for the
    // specified date.  'date' should be JavaScript Date instance.
    //
    // This method doesn't return anything, but rather calls the
    // callback function when the weather object is available. This
    // might be immediately or after some indeterminate amount of time.
    // The callback function should have two parameters.  The first
    // will be the index of the location and the second will be the
    // weather object for that location.
    //
    this.getWeatherAtIndexForDate = function(index, date, callback) {
        var latitude = locations[index].lat;
        var longitude = locations[index].lng;
        var theDate = date.darkSkyDateString();
        var darkskyKeyword = latitude+","+longitude+","+theDate; //the keyword to search the information needed
        
        if (locations[index].forecasts.hasOwnProperty(darkskyKeyword)===true){
            var store = locations[index].forecasts[darkskyKeyword]; //store the informations from darksky
            console.log(store) //display the informations
            callback(index,store); 
        }
        else {
            callbacks.index = {}
            callbacks.index.object = darkskyKeyword;
            callbacks.index.function = callback;
            //to check about information needed from the darksky using the latitude and longitude and the date user is looking at 
            var darksky = 'https://api.darksky.net/forecast/28c44e4248337124dbebaba73273e699/'+latitude+','+longitude+','+theDate; 
            var exclusion = "?exclude=minutely,hourly,currently&units=si" ; //things going to be excluded
            var callback = "&callback=locationWeather.weatherResponse" ;
            darksky += exclusion+callback
            var script = document.createElement('script');
            script.src = darksky;
            document.body.appendChild(script);
        }
    };

    // This is a callback function passed to darksky.net API calls.
    // This will be called via JSONP when the API call is loaded.
    //
    // This should invoke the recorded callback function for that
    // weather request.
    //
    this.weatherResponse = function(response) {
        
        var respond = response.daily.data[0];
        var lat = response.latitude;
        var lng = response.longitude;
        var index = indexForLocation(lat,lng)
        
        var call = callbacks.index.function;
        var key = callbacks.index.object;
        locations[index].forecasts[key] = respond;
        call(index,respond)
        
    };

    // Private methods:

    // Given a latitude and longitude, this method looks through all
    // the stored locations and returns the index of the location with
    // matching latitude and longitude if one exists, otherwise it
    // returns -1.
    //
    function indexForLocation(latitude, longitude)
    {
        for (var i=0; i<locations.length;++i){
            if(locations[i].lat === latitude && locations[i].lng === longitude){
                return(i)
            }
        }
        return(-1); //smaller to zero
    }
}

// Restore the singleton locationWeatherCache from Local Storage.
//

function loadLocations()
{
//make sure the local storage is available, else display alert
    if (localStorage===null){
        alert("local storage is currently not available");
    }
    else {
        var importData = localStorage.getItem(APP_PREFIX); //get the item from APP_PREFIX
        var convertToJSON = JSON.parse(importData); //parse the data
        locationWeather.initialiseFromPDO(convertToJSON); 
        }
    }


// Save the singleton locationWeatherCache to Local Storage.
//

//function to save location
function saveLocations()
{
    //stringify the data
    var ConvertJSON = JSON.stringify(locationWeather);

    //make sure the local storage is available, else display alert
    if (localStorage===null){ 
        alert("Local Storage not Available")
    }
    else {
     localStorage.setItem(APP_PREFIX,ConvertJSON);
    window.location.href = 'index.html'; 
    }
}
var locationWeather = new LocationWeatherCache();

if (localStorage.getItem(APP_PREFIX) !== null) {
    loadLocations();
}
        