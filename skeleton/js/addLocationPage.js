// Code for the Add Location page.
//define the variables
var DefaultNickname;
var latitude;
var longitude;
var map;
var geocoder;

//put the map on the screen page
  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644}, //put initial place on map
    zoom: 8
    });
    geocoder = new google.maps.Geocoder();      
};

function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('SearchLocation').value;
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
                });
            latitude = results[0].geometry.location.lat();
            longitude = results[0].geometry.location.lng();
            DefaultNickname = results[0].formatted_address;
    } 
        else {
            //display alert about that the map is not succesful and the reason
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}
                  
document.getElementById('SearchLocation').addEventListener('keydown', function(event) {
    if(event.keyCode == 13 || event.key === "Enter"){ //allow user to enter to search
        geocodeAddress(geocoder, map);
    }});

//allow user to give nickname to the map to be saved.
function saveLocation() {
    var nickname = document.getElementById('NicknameInput').value;
    if(nickname==='') {
        nickname=DefaultNickname;
    }
    locationWeather.addLocation(latitude,longitude,nickname)
    saveLocations();
       
}
