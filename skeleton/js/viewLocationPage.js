// Code for the View Location page.


// This is sample code to demonstrate navigation.
// You need not use it for final app.

//make sure the local storage is available, else display alert
if (localStorage === null){
    alert('local storage is currently not unavailable');
}
else {
    var locationIndex = localStorage.getItem(APP_PREFIX + "-selectedLocation"); 
}
    
    if (locationIndex !== null)
{

    var LocationMap = locationWeather.locationAtIndex(locationIndex);
}

var latitute = LocationMap.lat;
var longitute = LocationMap.lng;
var LocNickname = LocationMap.nick;
var day = new Date();
var daySlider = document.getElementById('slider')

//show map
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: latitute, lng: longitute},
    zoom: 8
    });
    //put marker on the map
    marker = new google.maps.Marker({
        position: {lat: latitute, lng: longitute},
        map: map
    })
}

locationWeather.getWeatherAtIndexForDate(locationIndex,day,locationForecastScreen);

function changeDate(){
    day = new Date();
    var minus = parseInt(daySlider.value);
    day.setDate(day.getDate()+minus);
    locationWeather.getWeatherAtIndexForDate(locationIndex,day,locationForecastScreen);
}

//function to remove location from the storage
function removeLocation(){
    var indexToRemove = locationIndex; //the object needs to be removed come from the variable called locationIndex
    locationWeather.removeLocationAtIndex(indexToRemove); 
    window.location.href = 'index.html'; 
}

function locationForecastScreen(index,response){
        
        //get all the informations to display from darksky
        var maxTemp = response.temperatureMax;
        var minTemp = response.temperatureMin;
        var humidity = response.humidity;
        var windSpeed = response.windSpeed;
        var icon = response.icon;
        var summaryWeather = response.summary;
    
        var monthNames = ["January","February","March","April","May","June","July","August","September","October","November","Desember"];
        year = day.getFullYear();
        month = monthNames[day.getMonth()];
    
        var maximTemp = document.getElementById('maximumTemp').innerHTML = 'Maximum : ' + ' ' + maxTemp + '&#8451;';
        var minimTemp = document.getElementById('minimumTemp').innerHTML = 'Minimum : ' + ' ' + minTemp + '&#8451;';
        var humidityLevel = document.getElementById('humid').innerHTML = 'Humidity : ' + ' ' + humidity + '%';
        var windVelocity = document.getElementById('WindSpeed').innerHTML = 'Wind Speed : ' + ' ' + windSpeed + 'm/s';
        var WeatherSymbol = document.getElementById('weatherIcon').innerHTML = '<img src=\images/' + icon + '.png width = "70px" height = "70px">';
        var summary = document.getElementById('summary').innerText = summaryWeather;
        var date = document.getElementById('weatherDate').innerHTML = day.getDate() + " " + month + " " + year; 
}

