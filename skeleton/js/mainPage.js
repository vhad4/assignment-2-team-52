// Code for the main app page (locations list).

// This is sample code to demonstrate navigation.
// You need not use it for final app.
var importData;
var printLocationList = document.getElementById('locationList');
var day = new Date();

function viewLocation(locationName)
{
    // Save the desired location to local storage
    localStorage.setItem(APP_PREFIX + "-selectedLocation", locationName);
    // And load the view location page.
    location.href = 'viewlocation.html';
}


function importToPrint() {
    //make sure the local storage is available, else display alert
    if(localStorage === null){
        alert("local storage is currently no available");
    }
    else {
     //parse the data
    importData = JSON.parse(localStorage.getItem(APP_PREFIX));
    }
}

function displaySummary(index,response)
{
    var icon = response.icon;
    var summary = response.summary;
    var newLi = document.createElement('li');
    newLi.className = "mdl-list__item mdl-list__item--two_line";
    newLi.setAttribute("onclick", "viewLocation("+index+")");
    printLocationList.appendChild(newLi);
    newLi.innerHTML = "<span class=\'mdl-list__item-primary-content\'><img class=\mdl-list__item-icon\' id=\'icon0\' src=\'images/"+icon+".png\' class=\'list-avatar\' /><span>"+importData[index].nick+"</span><span id=\'weather"+i+"\' class=\'mdl-list__item-sub-title\'>"+summary+"</span></span></li>"
}

importToPrint();

for (var i = 0; i<importData.length; i++) 
{
    locationWeather.getWeatherAtIndexForDate(i,day,displaySummary);
}

